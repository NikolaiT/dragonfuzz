from scapy.all import *

# Dot11      : 802.11
# Dot11ATIM  : 802.11 ATIM
# Dot11Ack   : 802.11 Ack packet
# Dot11AssoReq : 802.11 Association Request
# Dot11AssoResp : 802.11 Association Response
# Dot11Auth  : 802.11 Authentication
# Dot11Beacon : 802.11 Beacon
# Dot11Deauth : 802.11 Deauthentication
# Dot11Disas : 802.11 Disassociation
# Dot11Elt   : 802.11 Information Element
# Dot11ProbeReq : 802.11 Probe Request
# Dot11ProbeResp : 802.11 Probe Response
# Dot11QoS   : 802.11 QoS
# Dot11ReassoReq : 802.11 Reassociation Request
# Dot11ReassoResp : 802.11 Reassociation Response
# Dot11WEP   : 802.11 WEP packet

class Dot11EltRates(Packet):
    """ Our own definition for the supported rates field """
    name = "802.11 Rates Information Element"
    # Our Test STA supports the rates 6, 9, 12, 18, 24, 36, 48 and 54 Mbps
    supported_rates = [0x0c, 0x12, 0x18, 0x24, 0x30, 0x48, 0x60, 0x6c]
    fields_desc = [ByteField("ID", 1), ByteField("len", len(supported_rates))]
    for index, rate in enumerate(supported_rates):
        fields_desc.append(ByteField("supported_rate{0}".format(index + 1),
                                     rate))

packet = Dot11(
    addr1="9c:ef:d5:fc:0e:a8",
    addr2="c8:f7:33:d4:5a:e9",
    addr3="9c:ef:d5:fc:0e:a8") / Dot11AssoReq(
    cap=0x1100, listen_interval=0x00a) / Dot11Elt(
    ID=0, info="WPA3_Network")
packet /= Dot11EltRates()

#sendp(packet, iface="wlp0s29u1u7")
packet.show()

ack_packet = Dot11(
    addr1="9c:ef:d5:fc:0e:a8",
    addr2="c8:f7:33:d4:5a:e9",
    addr3="9c:ef:d5:fc:0e:a8") / Dot11Ack()

wireshark(ack_packet)