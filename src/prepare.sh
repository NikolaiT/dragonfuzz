#!/bin/bash
if [ -z "$1" ]
then
echo "please specify interface as first arg";
else
# use airmon to stop interfering processes
sudo airmon-ng check kill
# then stop network manager
# because airmon doesn't do a good job
sudo service network-manager stop
# enable hardware sim
#sudo modprobe mac80211_hwsim radios=3
rfkill unblock wifi
# Optionally kill other Wi-Fi clients the brute-for way:
sudo pkill wpa_supplicant
# Put the interface in monitor mode the old fashioned way
# Probably not necessary
sudo ifconfig $1 down
sudo iwconfig $1 mode monitor
sudo ifconfig $1 up
# To monitor traffic in wireshark you can execute
#sudo ifconfig hwsim0 up
fi
